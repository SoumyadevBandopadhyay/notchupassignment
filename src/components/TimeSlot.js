import React, { Component } from 'react'

class TimeSlot extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             times:[],
             timeSlots:[],
        }
        this.setinit=this.setinit.bind(this)
    }

    setinit(){
        this.setState({
            times:[],
            timeSlots:[],
        })
        this.props.stateLift()
    }
    
    componentDidMount(){
        let exactdate=this.props.date
        console.log(exactdate.setHours(new Date().getHours()))
        let startdate=exactdate.setMinutes(new Date().getMinutes())
        let enddate=exactdate.setHours(168)
        this.setState({
            times:this.props.timeslots,
            date:startdate,
            },()=>{
            this.setState({
                timeSlots: this.state.times.filter(each=>Number(each.slot)>=startdate && Number(each.slot)<=enddate)
                },()=>{
                console.log(this.state.times)
            })
        })
    }

    render() {
        return (
            <div>
                <div className="ContainerTimeSlot">
                    {this.state.timeSlots.map(e=>{
                        console.log(typeof e.slot)
                        return(
                            <div className="eachSlot">{e.slot}</div>
                        )
                    })}
                    
                </div>
                <div className="Continue">
                    <div class="buttons">
                        <button class="button is-link" onClick={this.setinit}>Submit</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default TimeSlot

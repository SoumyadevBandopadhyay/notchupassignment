import React, { Component } from 'react'
import Calendar from 'react-calendar'
import TimeSlot from './TimeSlot'
const key="JJmYc4tfM4PnXTM8bDoGoI3Exu_Qvt8qZB7XqxQSmU_H81jL_wplJVqLWPb2gQ1qwycfTiwvYmagpwvwfVq_mARXjcQxR4gdm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnC09Nb0QZ6ca_LU0vmo6mSiQ7SyFG3CgdL9-1Vgcha-TAYaAGhh-9xNG-9rMNEZHQRElvdDletx0"
const lib="MlJcTt87ug5f_XmzO-tnIbN3yFe7Nfhi6"

class Welcome extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            showCalender:false,
            date:"",
            dateString:"",
            Email:"",
            ContactNumber:"",
            showContinue:true,
            allSlots:[],
            course:"",
            slotArr:[],
            parentname:"",
            childname:"",
            childage:"",
        }

        this.showCalender=this.showCalender.bind(this)
        this.setDate=this.setDate.bind(this)
        this.setEmail=this.setEmail.bind(this)
        this.setNumber=this.setNumber.bind(this)
        this.setCourse=this.setCourse.bind(this)
        this.clickContinue=this.clickContinue.bind(this)
        this.initializeState=this.initializeState.bind(this)
        this.setparentname=this.setparentname.bind(this)
        this.setchildname=this.setchildname.bind(this)
        this.setchildage=this.setchildage.bind(this)
    }

    setchildage(e){
        this.setState({
            childage:e.target.value,
        })
    }

    setchildname(e){
        this.setState({
            childname:e.target.value,
        })
    }

    setparentname(e){
        this.setState({
            parentname:e.target.value
        })
    }

    initializeState(){
        console.log("Firing")
        this.setState({
            showCalender:false,
            date:"",
            dateString:"",
            Email:"",
            ContactNumber:"",
            showContinue:true,
            allSlots:[],
            course:"",
            slotArr:[],
            parentname:"",
            number:"",
            childname:"",
            childage:"",
        })
    }

    clickContinue(){
        this.state.allSlots.map((obj)=>{
            if(obj.course_name===this.state.course){
                this.setState({
                    slotArr: obj.slots,
                    showContinue: false,
                },()=>{
                    console.log(this.state.slotArr)
                })
            }
            return obj
        })
    }

    setCourse(e){
        this.setState({
            course: e.target.value,
        })
    }

    setEmail(e){
        this.setState({
            Email:e.target.value,
        })
    }

    setNumber(e){
        this.setState({
            ContactNumber: e.target.value,
        })
    }
    
    showCalender(){
        this.setState({
            showCalender: !this.state.showCalender,
        })
    }

    setDate(date){
        this.setState({date},()=>{
            this.setState({
                dateString: this.state.date.getDate()+"/"+this.state.date.getMonth()+"/"+this.state.date.getFullYear()
            })
        })
    }

    componentDidMount(){
        fetch(`https://script.googleusercontent.com/macros/echo?user_content_key=${key}&lib=${lib}`,{
            method: "GET",
        }).then(res=>{
            if(res.status===200){
                return res.json()
            }
        }).then(result=>{
            this.setState({
                allSlots: result,
            })

        })
    }

    render() {
        return (
            <div>
                <p className="title">Fill up the form to book a trial class</p>
                <div className="Form">

                    <div className="formElements">
                        <div class="field">
                        <label class="label">Parents Name</label>
                        <div class="control">
                            <input class="input" type="text" placeholder="Parents Name" onChange={this.setparentname} value={this.state.parentname}/>
                        </div>
                        </div>

                        <div class="field">
                        <label class="label">Parent's Contact Number</label>
                        <div class="control">
                            <input class="input" type="text" placeholder="Enter Contact Number" onChange={this.setNumber} value={this.state.ContactNumber}/>
                        </div>
                        </div>

                        <div class="field">
                        <label class="label">Email</label>
                        <div class="control has-icons-left has-icons-right">
                            <input class="input" type="email" placeholder="Enter Email" onChange={this.setEmail} value={this.state.Email}/>
                            <span class="icon is-small is-left">
                            <i class="fas fa-envelope"></i>
                            </span>
                            {/* <span class="icon is-small is-right">
                            <i class="fas fa-exclamation-triangle"></i>
                            </span> */}
                        </div>
                        {/* <p class="help is-danger">This email is invalid</p> */}
                        </div>

                        <div class="field">
                        <label class="label">Child's Name</label>
                        <div class="control">
                            <input class="input" type="text" placeholder="Child's Name" onChange={this.setchildname} value={this.state.childname}/>
                        </div>
                        </div>
                    </div>

                    <div className="formElements">
                        <div class="field">
                        <label class="label">Child's Age</label>
                        <div class="control">
                            <input class="input" type="text" placeholder="Age" onChange={this.setchildage} value={this.state.childage}/>
                        </div>
                        </div>

                        <div class="field">
                        <label class="label">Course Name</label>
                        <div class="control">
                            <div class="select">
                            <select onChange={this.setCourse} className="dropDown">
                                <option>Select dropdown</option>
                                {this.state.allSlots.map(obj=>{
                                    return(
                                        <option>{obj.course_name}</option>
                                    )
                                })}
                            </select>
                            </div>
                        </div>
                        </div>
                        <div>
                            <div>
                                <label class="label">Date</label>
                                <input className="input" type="text" placeholder="Date" value={this.state.dateString}/>
                                <button onClick={this.showCalender}>
                                    <i  className="far fa-calendar-alt " style={{fontSize:"30px"}}></i>
                                </button>
                            </div>
                            {this.state.showCalender?
                            <Calendar onChange={this.setDate} value={this.state.date}/>
                            :
                            <></>
                            }
                        </div>
                    </div>
                </div>
                {this.state.showContinue? 
                <div className="Continue">
                    <div class="buttons">
                        <button class="button is-link" onClick={this.clickContinue}>Continue</button>
                    </div>
                </div>
                :
                <></>
                }
                
                <div>
                    {this.state.slotArr.length>0 ?
                        <TimeSlot date={this.state.date} timeslots={this.state.slotArr} stateLift={this.initializeState}/>
                    :
                    <></>
                    }
                </div>
            </div>
        )
    }
}

export default Welcome
